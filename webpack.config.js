const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const pluginName = 'codeblock'

const config = {
  entry: {
    'plugin': './plugins/codemirror/plugin.js',
    'plugin.min': './plugins/codemirror/plugin.js',
  },

  output: {
    publicPath: '/',
    path: path.join(__dirname, './dist'),
    filename: '[name].js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    },
    {
      test: /\.css$/,
      use: [
        'style-loader',
        'css-loader'
      ]
    }]
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './plugins/codemirror/langs', to: 'langs' },
    ]),
  ],
}


module.exports = (env, argv) => {

  if (argv.mode === 'production') {
    config.plugins.push(
      new CopyWebpackPlugin([{
        from: path.join(__dirname, './LICENSE'),
        to: path.join(__dirname, './dist', pluginName)
      }])
    )
  }

  return config
}